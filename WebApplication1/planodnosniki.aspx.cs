﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class planodnosniki : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList1.SelectedValue == "v1a")
            {
                MultiView1.SetActiveView(View1);
            }
            else if (DropDownList1.SelectedValue == "v1b")
            {
                MultiView1.SetActiveView(View2);
            }
            else if (DropDownList1.SelectedValue == "v1c")
            {
                MultiView1.SetActiveView(View3);
            }
            else if (DropDownList1.SelectedValue == "v2a")
            {
                MultiView1.SetActiveView(View4);
            }
            else if (DropDownList1.SelectedValue == "v2b")
            {
                MultiView1.SetActiveView(View5);
            }
            else if (DropDownList1.SelectedValue == "v2c")
            {
                MultiView1.SetActiveView(View6);
            }
            else if (DropDownList1.SelectedValue == "v3a")
            {
                MultiView1.SetActiveView(View7);
            }
            else if (DropDownList1.SelectedValue == "v3b")
            {
                MultiView1.SetActiveView(View8);
            }
            else if (DropDownList1.SelectedValue == "v3c")
            {
                MultiView1.SetActiveView(View9);
            }
            else
                MultiView1.ActiveViewIndex = -1;
        }
    }
}