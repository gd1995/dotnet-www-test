﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormMasterPage.Master" AutoEventWireup="true" CodeBehind="rekt1.aspx.cs" Inherits="WebApplication1.rekt1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
                 <asp:ValidationSummary
        HeaderText="Podczas wypełniana ankiety wystąpił jeden lub więcej błędów:"
        DisplayMode="BulletList"
        EnableClientScript="true"
        runat="server"/>

            Imię:&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbImie" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Imie: To pole jest wymagane" ControlToValidate ="tbImie"  CssClass="error"  >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Imie: Nieprawidłowy format" ControlToValidate ="tbImie"  CssClass="error" ValidationExpression= "^([A-ZĄĘŁŃÓŚŹŻ]([a-ząćęłńóśźż])+)( [A-ZĄĘŁŃÓŚŹŻ]([a-ząćęłńóśźż])+)?" >*</asp:RegularExpressionValidator>
        <br />
        Nazwisko:&nbsp;&nbsp;&nbsp; <asp:TextBox ID="tbNazwisko" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Nazwisko: To pole jest wymagane" ControlToValidate ="tbNazwisko"  CssClass="error"  >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Nazwisko: Nieprawidłowy format" ControlToValidate ="tbNazwisko"  CssClass="error" ValidationExpression= "^([A-ZĄĘŁŃÓŚŹŻ]([a-ząćęłńóśźż])+)(-[A-ZĄĘŁŃÓŚŹŻ]([a-ząćęłńóśźż])+)?" >*</asp:RegularExpressionValidator>        
        <br />
        Adres email:&nbsp;&nbsp;&nbsp;
        <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="E-mail: To pole jest wymagane" ControlToValidate ="tbEmail"  CssClass="error"  >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="E-mail: Nieprawidłowy format" ControlToValidate="tbEmail" CssClass="error" ValidationExpression= "^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$"   >*</asp:RegularExpressionValidator>
        <br />
        <br />
        <br />
        <asp:Button ID="Button2" runat="server" OnClick="Button2_Click" Text="Dalej" />
</asp:Content>
