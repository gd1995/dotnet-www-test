﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class rekt3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            lbDane.Text = getImie() + " " + getNazwisko();
            lbEmail.Text = getEmail();
            lbKierunek.Text = getKierunek();
            lbAtuty.Text = getAtuty();
        }

        private string getImie()
        {
            return Request.QueryString["imie"];
        }
        private string getNazwisko()
        {
            return Request.QueryString["nazwisko"];
        }

        private string getEmail()
        {
            return Request.QueryString["email"];
        }

        private string getKierunek()
        {
            return Request.QueryString["rb"];
        }
        private string getAtuty()
        {
            string egzamin;
            egzamin = "Część humanistyczna: " + Request.QueryString["hum"] + ", Część matematyczno-przyrodnicza: " + Request.QueryString["mat"] + ", Język obcy nowożytny: " + Request.QueryString["ob"];
            return egzamin;
        }

        protected void Button4_Click(object sender, EventArgs e)
        {
            Response.Redirect("rekt4.aspx");
        }
    }
}