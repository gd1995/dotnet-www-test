﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class rekt1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        }
        protected void Button2_Click(object sender, EventArgs e)
        {
            string url = "rekt2.aspx";
            string parameters;
            parameters = "?";
            parameters += "imie=" + tbImie.Text;
            parameters += "&";
            parameters += "nazwisko=" + tbNazwisko.Text;
            parameters += "&";
            parameters += "email=" + tbEmail.Text;
            Response.Redirect(url + parameters);
        }
    }
}