﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormMasterPage.Master" AutoEventWireup="true" CodeBehind="rekt2.aspx.cs" Inherits="WebApplication1.rekt2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
                 <asp:ValidationSummary
        HeaderText="Podczas wypełniana ankiety wystąpił jeden lub więcej błędów:"
        DisplayMode="BulletList"
        EnableClientScript="true"
        runat="server"/>

     Ubiegasz się o status ucznia naszego liceum.<br /><br />

        Na jaki profil chciałbyś aplikować?:&nbsp;&nbsp;&nbsp;&nbsp;<br /><br />

        <asp:RadioButtonList ID="RadioButtonList1" runat="server" align="center">
            <asp:ListItem Selected="True">Humanistyczny</asp:ListItem>
            <asp:ListItem>Matematyczno-fizyczny</asp:ListItem>
            <asp:ListItem>Biologiczno-chemiczny</asp:ListItem>
        </asp:RadioButtonList>

        <br /><br />
        Jaki miałeś wynik punktowy na egzaminie gimnazjalnym?
        <br /><br />
        Część humanistyczna:
        <br /><br />
        <asp:TextBox ID="tbHum" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="To pole jest wymagane" ControlToValidate ="tbHum"  CssClass="error"  >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ErrorMessage="Akceptowalny zakres: 0-50" ControlToValidate ="tbHum"  CssClass="error" ValidationExpression= "^[0-9][0-9]?$|^50$" >*</asp:RegularExpressionValidator>       
        <br />
                  <br />  Część matematyczno-przyrodnicza:
        <br /><br />
        <asp:TextBox ID="tbMat" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="To pole jest wymagane" ControlToValidate ="tbMat"  CssClass="error"  >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ErrorMessage="Akceptowalny zakres: 0-50" ControlToValidate ="tbMat"  CssClass="error" ValidationExpression= "^[0-9][0-9]?$|^50$" >*</asp:RegularExpressionValidator>       
        <br /><br />
                    Język obcy nowożytny:
        <br /><br />
        <asp:TextBox ID="tbOb" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="To pole jest wymagane" ControlToValidate ="tbOb"  CssClass="error"  >*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Akceptowalny zakres: 0-50" ControlToValidate ="tbOb"  CssClass="error" ValidationExpression= "^[0-9][0-9]?$|^50$" >*</asp:RegularExpressionValidator>       
        <br /><br />
        <br />

        <asp:Button ID="Button3" runat="server" OnClick="Button3_Click" Text="Dalej" />    


</asp:Content>
