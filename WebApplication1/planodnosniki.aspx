﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormMasterPage.Master" AutoEventWireup="true" CodeBehind="planodnosniki.aspx.cs" Inherits="WebApplication1.planodnosniki" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br /><br />
        Wybierz klasę:&nbsp;
        <asp:DropDownList ID="DropDownList1" runat="server" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AutoPostBack="True">
            <asp:ListItem Value="v0">----</asp:ListItem>
            <asp:ListItem Value="v1a">IA</asp:ListItem>
            <asp:ListItem Value="v1b">IB</asp:ListItem>
            <asp:ListItem Value="v1c">IC</asp:ListItem>
            <asp:ListItem Value="v2a">IIA</asp:ListItem>
            <asp:ListItem Value="v2b">IIB</asp:ListItem>
            <asp:ListItem Value="v2c">IIC</asp:ListItem>
            <asp:ListItem Value="v3a">IIIA</asp:ListItem>
            <asp:ListItem Value="v3b">IIIB</asp:ListItem>
            <asp:ListItem Value="v3c">IIIC</asp:ListItem>
        </asp:DropDownList>


    <asp:MultiView ID="MultiView1" runat="server">
        <asp:View ID="View1" runat="server">
           <br /> Plan klasy IA niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View2" runat="server">
          <br />   Plan klasy IB niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View3" runat="server">
          <br />   Plan klasy IC niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View4" runat="server">
           <br />  Plan klasy IIA niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View5" runat="server">
           <br />  Plan klasy IIB niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View6" runat="server">
           <br />  Plan klasy IIC niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View7" runat="server">
           <br />  Plan klasy IIIA niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View8" runat="server">
           <br />  Plan klasy IIIB niedostępny. Przepraszamy za usterki.
        </asp:View>
        <asp:View ID="View9" runat="server">
          <br />   Plan klasy IIIC niedostępny. Przepraszamy za usterki.
        </asp:View>
    </asp:MultiView>

</asp:Content>
