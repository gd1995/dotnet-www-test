﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormMasterPage.Master" AutoEventWireup="true" CodeBehind="Kontakt.aspx.cs" Inherits="WebApplication1.Kontakt" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    <br />
        <asp:ValidationSummary
            HeaderText="Podczas wypełniana formularza kontaktowego wystąpił jeden lub więcej błędów:"
            DisplayMode="BulletList"
            EnableClientScript="true"
            runat="server" />
        <asp:Label ID="Label1" runat="server" Text="Aby się z nami skontaktować wypełnij poniższy formularz:"></asp:Label>
        <br />
        <br />
        <asp:Label ID="Label2" runat="server" Text="Imię i nazwisko:"></asp:Label>
        <br />
        <asp:TextBox ID="tbPrzedstaw" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Imie i nazwisko: To pole jest wymagane" ControlToValidate="tbPrzedstaw" CssClass="error">*</asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label ID="Label3" runat="server" Text="Adres e-mail:"></asp:Label>
        <br />
        <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="E-mail: To pole jest wymagane" ControlToValidate="tbEmail" CssClass="error">*</asp:RequiredFieldValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="E-mail: Nieprawidłowy format" ControlToValidate="tbEmail" CssClass="error" ValidationExpression="^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$">*</asp:RegularExpressionValidator>
        <br />
        <br />
        <asp:Label ID="Label4" runat="server" Text="Temat:"></asp:Label>
        <br />
        <asp:TextBox ID="tbTemat" runat="server"></asp:TextBox>
        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Temat: To pole jest wymagane" ControlToValidate="tbTemat" CssClass="error">*</asp:RequiredFieldValidator>
        <br />
        <br />
        <asp:Label ID="Label5" runat="server" Text="Wiadomość:"></asp:Label>
        <br />
        <div id="textboxwiadomosc">
            <textarea cols="50" rows="10" name="text" id="contact_text" class="inputbox required"></textarea>
        </div>
        <br />
        <br />
        <asp:Button ID="Button1" runat="server" Text="Wyślij" OnClick="Button1_Click" />
        <br />

</asp:Content>
