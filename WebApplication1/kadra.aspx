﻿<%@ Page Title="" Language="C#" MasterPageFile="~/WebFormMasterPage.Master" AutoEventWireup="true" CodeBehind="kadra.aspx.cs" Inherits="WebApplication1.kadra" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <br />
    <div id="right">
    <asp:GridView ID="Grid1" runat="server" AutoGenerateColumns="False" DataSourceID="XmlDataSource1" Width="471px" DataKeyNames="ID" OnRowCommand="Grid1_RowCommand" OnSelectedIndexChanged="Grid1_SelectedIndexChanged">

        <Columns>
            <asp:TemplateField HeaderText="Imie i nazwisko:">
                <ItemTemplate>
                    <%# XPath("Name") %>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:CommandField ShowSelectButton="true" SelectText="Zobacz szczegóły" />
        </Columns>
    </asp:GridView>
    <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/Teachers.xml"></asp:XmlDataSource>
    </div>
    <br />
    <br />
    <div id="left">
    <asp:DetailsView ID="details" runat="server" DataSourceID="XmlDataSource2" AutoGenerateRows="false" GridLines="None">
        <FieldHeaderStyle  />
        <RowStyle  />
        <Fields>
            <asp:TemplateField HeaderText="Imię i nazwisko:">
                <ItemTemplate>
                    <%# XPath("Name") %>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Przedmioty:">
                <ItemTemplate>
                    <asp:DataList ID="DataList1" runat="server" DataSource='<%# XPathSelect("Subjects/Subject") %>'>
                        <ItemTemplate>
                            <%# XPath(".") %>
                            <br />
                        </ItemTemplate>

                    </asp:DataList>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Informacje dodatkowe:">
                <ItemTemplate>
                    <%# XPath("ExtraInfo") %>
                </ItemTemplate>
            </asp:TemplateField>
        </Fields>
    </asp:DetailsView>
    <asp:XmlDataSource ID="XmlDataSource2" runat="server" DataFile="~/Teachers.xml"></asp:XmlDataSource>
    </div>
    <br />


</asp:Content>
