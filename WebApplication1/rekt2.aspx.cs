﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class rekt2 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.UnobtrusiveValidationMode = System.Web.UI.UnobtrusiveValidationMode.None;
        }
        private string getImie()
        {
            return Request.QueryString["imie"];
        }


        private string getNazwisko()
        {
            return Request.QueryString["nazwisko"];
        }


        private string getEmail()
        {
            return Request.QueryString["email"];
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            string url;
            string parametry;

            url = "rekt3.aspx";
            parametry = "?";
            parametry += "imie=" + getImie();
            parametry += "&";
            parametry += "nazwisko=" + getNazwisko();
            parametry += "&";
            parametry += "email=" + getEmail();
            parametry += "&";
            parametry += "hum=" + tbHum.Text;
            parametry += "&";
            parametry += "mat=" + tbMat.Text;
            parametry += "&";
            parametry += "ob=" + tbOb.Text;
            parametry += "&";
            parametry += "rb=" + RadioButtonList1.SelectedValue;

            Server.Transfer(url + parametry);
        }
    }
}